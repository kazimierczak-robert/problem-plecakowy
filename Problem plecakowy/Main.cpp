//Wykonano na podstawie: https://rosettacode.org/wiki/Knapsack_problem/0-1#C

#include "string"
#include "vector"
#include "fstream"
#include "iostream"
#include <omp.h>
using namespace std;

struct Przedmiot
{
	string nazwa;
	int waga;
	int wartosc;
};

bool *knapsack(vector <Przedmiot> wektor, int ilosc_przedmiotow, int pojemnosc_plecaka, double &start, double &stop)
{
	int i, j, a, b, *mm, **m;
	bool *czy_zapakowano;
	mm = (int*)calloc((ilosc_przedmiotow + 1) * (pojemnosc_plecaka + 1), sizeof(int));
	m = (int**)malloc((ilosc_przedmiotow + 1) * sizeof(int *));
	m[0] = mm;
	start = omp_get_wtime();
	for (i = 1; i <= ilosc_przedmiotow; i++) 
	{
		m[i] = &mm[i * (pojemnosc_plecaka + 1)];

		#pragma omp parallel for shared(pojemnosc_plecaka,wektor,m), private(j,a,b)
		for (j = 0; j <= pojemnosc_plecaka; j++) 
		{
			if (wektor[i - 1].waga > j) 
			{
				m[i][j] = m[i - 1][j];
			}
			else 
			{
				a = m[i - 1][j];
				b = m[i - 1][j - wektor[i - 1].waga] + wektor[i - 1].wartosc;
				m[i][j] = a > b ? a : b;
			}
		}
	}
	czy_zapakowano = (bool*)calloc(ilosc_przedmiotow, sizeof(bool));
	for (i = ilosc_przedmiotow, j = pojemnosc_plecaka; i > 0; i--) 
	{
		if (m[i][j] > m[i - 1][j]) 
		{
			czy_zapakowano[i - 1] = 1;
			j -= wektor[i - 1].waga;
		}
	}
	stop = omp_get_wtime();
	free(mm);
	free(m);
	return czy_zapakowano;
}

int main()
{
	fstream plik;
	plik.open("Input.txt", ios::in);
	if (plik.good() == false)
	{
		cout << "Problem z otwarciem pliku z danymi!" << endl;
		return 1;
	}

	vector <Przedmiot> wektor;
	Przedmiot tmp;
	//cout << "LISTA PRZEDMIOTOW" << endl;
	//cout << "Waga" << "\t" << "Wartosc" << "\t" << "Nazwa" << endl;
	while (!plik.eof())
	{
		plik >> tmp.nazwa;
		plik >> tmp.waga;
		plik >> tmp.wartosc;
		wektor.push_back(tmp);
		//cout << wektor[wektor.size() - 1].waga << "\t" << wektor[wektor.size() - 1].wartosc << "\t" << wektor[wektor.size() - 1].nazwa << endl;
	}
	//cout << endl;
	plik.close();

	int ilosc_przedmiotow = 0;
	ilosc_przedmiotow = wektor.size();
	if (ilosc_przedmiotow <= 0)
	{
		cout << "Brak przedmiotow w pliku!" << endl;
		return 1;
	}

	int pojemnosc_plecaka;
	cout << "Podaj pojemnosc plecaka: ";
	cin >> pojemnosc_plecaka;
	if (pojemnosc_plecaka <= 0)
	{
		cout << "Podano zla wartosc!" << endl;
		return 1;
	}
	
	int laczna_waga = 0, laczna_wartosc = 0;
	bool *czy_zapakowano;

	double start = 0;
	double stop = 0;

	czy_zapakowano = knapsack(wektor, ilosc_przedmiotow, pojemnosc_plecaka, start, stop);

	cout <<endl<< "ROZWIAZANIE:" << endl;
	cout << "Waga" << "\t" << "Wartosc" << "\t" << "Nazwa" << endl;
	for (int i = 0; i < ilosc_przedmiotow; i++) 
	{
		if (czy_zapakowano[i])
		{
			cout << wektor[i].waga << "\t" << wektor[i].wartosc << "\t" << wektor[i].nazwa << endl;
			laczna_waga += wektor[i].waga;
			laczna_wartosc += wektor[i].wartosc;
		}
	}
	cout << laczna_waga << "\t" << laczna_wartosc << "\t" << "W SUMIE" << endl;
	cout << "Czas dzialania algorytmu - problem plecakowy w ms: " << (stop - start)*1000 << endl;
	return 0;
}